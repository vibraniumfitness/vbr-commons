import moment from 'moment';
import * as DeviceInfo from 'react-native-device-info';

const ISODateTimeFormat = 'YYYY-MM-DD HH:mm:ss';

// TODO: probably name this to 'fromUTC'
const convertUTCToLocal = (
    dateTime,
    inputFormat = ISODateTimeFormat,
    outputFormat = ISODateTimeFormat
) => {
    let utcDateTime = moment.utc(dateTime, inputFormat);
    let localDateTime = utcDateTime.clone().local();
    return localDateTime.format(outputFormat);
};

// TODO: probably name this to 'toUTC'
const convertLocalToUTC = (
    dateTime,
    inputFormat = ISODateTimeFormat,
    outputFormat = ISODateTimeFormat
) => {
    let localDateTime = moment(dateTime, inputFormat);
    return localDateTime
        .clone()
        .utc()
        .format(outputFormat);
};

// TODO: the deviceLocale is not working in the app debug or simulator mode.
// Need some extra stuff for proper linking of the device info library.
const convertDateTimeToDeviceLocale = (
    dateTime,
    inputFormat = ISODateTimeFormat,
    outputFormat = 'DD MMMM HH:mm a'
) => {
    let deviceLocale = DeviceInfo.getBatteryLevel(); // fr
    let dateTimeStd = moment(dateTime, inputFormat); // 27-01-2020 10:30:00
    moment.locale(deviceLocale);
    return dateTimeStd.clone().format(outputFormat); // 27 Janvier 10:30 am
};

const plainTextDateTime = dateTime => {
    return moment(dateTime).format('DD MMMM HH:mm a'); // 27 January 10:30 am
};

const getDate = dateTime => {
    return moment(dateTime).format('DD MMMM'); // 27 January
};

const getTime = dateTime => {
    return moment(dateTime).format('HH:mm a'); // 10:30 am
};

// wrapper function to avoid importing moment in the components again
const convertTimeFormat = (
    dateTime,
    inputFormat = ISODateTimeFormat,
    outputFormat = ISODateTimeFormat
) => {
    return moment(dateTime, inputFormat)
        .clone()
        .format(outputFormat);
};

const startOfDay = (
    dateTime,
    inputFormat = ISODateTimeFormat,
    outputFormat = ISODateTimeFormat
) => {
    let dateTimeOriginal = moment(dateTime, inputFormat);
    let dateTimeISO = moment(dateTimeOriginal.clone().format(ISODateTimeFormat), ISODateTimeFormat);
    return dateTimeISO
        .clone()
        .startOf('day')
        .format(outputFormat);
};

const endOfDay = (dateTime, inputFormat = ISODateTimeFormat, outputFormat = ISODateTimeFormat) => {
    let dateTimeOriginal = moment(dateTime, inputFormat);
    let dateTimeISO = moment(dateTimeOriginal.clone().format(ISODateTimeFormat), ISODateTimeFormat);
    return dateTimeISO
        .clone()
        .endOf('day')
        .format(outputFormat);
};

export {
    convertUTCToLocal,
    convertLocalToUTC,
    convertDateTimeToDeviceLocale,
    plainTextDateTime,
    getDate,
    getTime,
    convertTimeFormat,
    startOfDay,
    endOfDay
};
